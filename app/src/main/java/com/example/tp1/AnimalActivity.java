package com.example.tp1;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;


public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal);

        TextView nom = (TextView) findViewById(R.id.showNom);
        Intent intent = getIntent();
        String name = intent.getStringExtra("nom");
        nom.setText(name);

        final Animal animal = AnimalList.getName(name);

        ImageView avatar = (ImageView) findViewById(R.id.avatar);
        TextView vie = (TextView) findViewById(R.id.vie);
        TextView gestation = (TextView) findViewById(R.id.gest);
        TextView PN = (TextView) findViewById(R.id.poidsN);
        TextView PA = (TextView) findViewById(R.id.poidsA);
        final EditText cons = (EditText) findViewById(R.id.conservation);
        Button save = (Button)findViewById(R.id.button);

        avatar.setImageResource(getResources().getIdentifier(animal.getImgFile(), "drawable", getPackageName()));
        vie.setText(animal.getHightestLifespan()+" années");
        gestation.setText(animal.getGestationPeriod()+" jours");
        PN.setText(animal.getStrBirthWeight());
        PA.setText(animal.getAdultWeight()+" kg");
        cons.setText(animal.getConservationStatus());

        save.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
            animal.setConservationStatus(cons.getText().toString());
            finish();

            }
        });

    }

}