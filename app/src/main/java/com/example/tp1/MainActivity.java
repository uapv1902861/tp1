package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    Intent myIntent;
    String[] items = AnimalList.getNameArray();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView listView = (ListView) findViewById(R.id.listView);



        ArrayAdapter adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, items);

        listView.setAdapter(adapter);

       myIntent = new Intent(this, AnimalActivity.class);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String name = listView.getItemAtPosition(position).toString();
                myIntent.putExtra("nom", name);
                startActivity(myIntent);
            }
        });


      /* final RecyclerView rv = (RecyclerView) findViewById(R.id.list);

       rv.setAdapter(new IconicAdapter());
        */

    }
    /*
    class IconicAdapter extends RecyclerView.Adapter<RowHolder>{

        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType){
            return(new RowHolder(getLayoutInflater().inflate(R.layout.activity_recycler, parent, false)));
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int pos){
            holder.bindModel(items[pos], );
        }

        @Override
        public int getItemCount(){
            return(items.length);
        }
    }

    static class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView label=null;
        ImageView icon=null;

        RowHolder(View row){
            super(row);

            label=(TextView)row.findViewById(R.id.animal);
            icon=(ImageView)row.findViewById(R.id.image);
        }

        @Override
        public void onClick(View v){
            myIntent = new Intent(MainActivity, AnimalActivity.class);
            String name = listView.getItemAtPosition(pos).toString();
            myIntent.putExtra("nom", name);
            startActivity(myIntent)
        }

        void bindModel(String item){
            label.setText(item);
            icon.setImageResource(R.drawable.);
        }
    }*/
}

